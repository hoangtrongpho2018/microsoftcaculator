import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';


import React, { Component } from 'react';
import uuid from "uuid";
import Header from '../components/body/header';
import MainContent from '../components/body/mainContent';
import TabItem from '../components/tab/tabItem';

class Body extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      tabItemData: [
        {
          id: uuid(),
          name: "Your Estimate 1",
        },
        {
          id: uuid(),
          name: "Your Estimate 2222",
        },
      ],
      listSelectedProduct: new Array(),
      id: uuid(),  
    }
  }
  public addProductAction(selectedProduct: any){
    let listSelectedProduct = this.state.listSelectedProduct;
    let newSelectedProduct = {...selectedProduct, id: uuid()};
    selectedProduct = newSelectedProduct;
    listSelectedProduct.push(selectedProduct);
    this.setState({listSelectedProduct});
    console.log(listSelectedProduct)
  }
  render() {
    return (
      <div className="container-fluid">
        <Header 
        />
        <div className="tab-content" id="pills-tabContent">
          <MainContent />
          {
          this.state.tabItemData.map((item: { id: React.ReactText; }) => 
            <TabItem key={item.id} item={item} addProduct={()=>this.addProductAction(item)}  />)
          }
          {
          this.state.listSelectedProduct.map((item: any,index: number) => 
            <div className="row">
              <div>{item.name? item.name : ""}</div>
            </div>
          )}
        </div>
        
      </div>

    );
  }
}

export default Body;
