import React, {useState} from "react"
import icon1 from "../../image/icon1.png"
import icon2 from "../../image/icon2.png"
import icon3 from "../../image/icon3.png"
import uuid from "uuid";

const featureData=[
      {
        id:parseInt("1"),
        iconName:icon1,
        productName:"Virtual Machines",
        title: "1 D2 v3 (2 vCPU(s), 8 GB RAM) x 730 Hours; Windows – (OS Only); Pay as you go; 0 managed OS disks – S4, 100 transaction units",
        content:"Provision Windows and Linux virtual machines in seconds",
        isCollapse:false,
        price: Number("152.62"),
      },
      {
        id:parseInt("2"),
        iconName:icon2,
        productName:"Virtual Machines",
        title: "1 D2 v3 (2 vCPU(s), 8 GB RAM) x 730 Hours; Windows – (OS Only); Pay as you go; 0 managed OS disks – S4, 100 transaction units",
        content:"Provision Windows and Linux virtual machines in seconds",
        isCollapse:false,
        price: Number("152.62"),
      },
      {
        id:parseInt("3"),
        iconName:icon3,
        productName:"Virtual Machines",
        title: "1 D2 v3 (2 vCPU(s), 8 GB RAM) x 730 Hours; Windows – (OS Only); Pay as you go; 0 managed OS disks – S4, 100 transaction units",
        content:"Provision Windows and Linux virtual machines in seconds",
        isCollapse:false,
        price: Number("152.62"),
      },
]
export default featureData