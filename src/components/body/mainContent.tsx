import React, { Component } from 'react';

import CardItem from '../body/CardItem'
import featureData from '../productData/featureData' 
import uuid from "uuid";

export class MainContent extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
    featureProduct: featureData,
    listSelectedProduct: new Array(),
    id: uuid(),  
    };
  
  }

  public addProductAction(selectedProduct: any){
    debugger
    let listSelectedProduct = this.state.listSelectedProduct;
    let newSelectedProduct = {...selectedProduct, id: uuid()};
    selectedProduct = newSelectedProduct;
    listSelectedProduct.push(selectedProduct);
    this.setState({listSelectedProduct});
  }

  render() {
    return (
        <div className="tab-pane fade show active box table2" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <div className="tab-title all-content">Select a product to include it in your estimate.</div>
        <div className="tab-content col-12">   
          <div className="row">
            <div className="product-search-container">
              <input className="product-search" aria-label="Search products" placeholder="Search products" ></input>
              <button className="clear-search" aria-label="Clear search" type="button" data-event="page-clicked-button" data-bi-id="page-clicked-button" data-bi-area="content">×</button>
            </div>
          </div>
          <div className="row">
            <div className="nav flex-column nav-pills col-2 color" id="v-pills-tab" role="tablist" aria-orientation="vertical">
              <a className="nav-link active" id="v-pills-featured-tab" data-toggle="pill" href="#v-pills-featured" role="tab" aria-controls="v-pills-featured" aria-selected="true">Featured</a>
              <a className="nav-link" id="v-pills-compute-tab" data-toggle="pill" href="#v-pills-compute" role="tab" aria-controls="v-pills-compute" aria-selected="false">Compute</a>
              <a className="nav-link" id="v-pills-networking-tab" data-toggle="pill" href="#v-pills-networking" role="tab" aria-controls="v-pills-networking" aria-selected="false">Networking</a>
              <a className="nav-link" id="v-pills-storage-tab" data-toggle="pill" href="#v-pills-storage" role="tab" aria-controls="v-pills-storage" aria-selected="false">Storage</a>
              <a className="nav-link" id="v-pills-web-tab" data-toggle="pill" href="#v-pills-web" role="tab" aria-controls="v-pills-web" aria-selected="false">Web</a>
            </div>
            {/* <!--Products--> */}
            <div className="tab-content col-10" id="v-pills-tabContent">
              <div className="tab-pane fade show active" id="v-pills-featured" role="tabpanel" aria-labelledby="v-pills-featured-tab">
                <div className="row">
                
                {this.state.featureProduct.map((item: { id: React.ReactText; }) => <CardItem key={item.id}  item={item} addProduct={()=>this.addProductAction(item)} 
                    />)}
                  
                </div>
              </div>
              <div className="tab-pane fade" id="v-pills-networking" role="tabpanel" aria-labelledby="v-pills-networking-tab">Networking</div>
              <div className="tab-pane fade" id="v-pills-storage" role="tabpanel" aria-labelledby="v-pills-storage-tab">Storage </div>
              <div className="tab-pane fade" id="v-pills-web" role="tabpanel" aria-labelledby="v-pills-web-tab">Web</div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default MainContent;
